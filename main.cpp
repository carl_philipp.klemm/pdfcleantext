/**
* pdfwordcounter
* Copyright (C) 2022 Carl Klemm
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License
* version 3 as published by the Free Software Foundation.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the
* Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
* Boston, MA  02110-1301, USA.
*/

#include <iostream>
#include <filesystem>
#include <memory>
#include <poppler-document.h>
#include <poppler-page.h>
#include <poppler-global.h>
#include <sstream>
#include <cassert>
#include <fstream>
#include <ctime>
#include <algorithm>
#include <map>
#include <algorithm>

#include "log.h"
#include "tokenize.h"


static void printUsage(int argc, char** argv)
{
	Log(Log::WARN)<<"An application that, given a directory of pdf files, will generate a text file containing all the text of those files";
	Log(Log::WARN)<<"Usage: "<<argv[0]<<" [FOLDER or DOCUMENT] [OUTPUT FILE]";
}

static void dropMessage(const std::string& message, void* userdata)
{
	(void)message;
	(void)userdata;
}

static std::string getText(poppler::document* document)
{
	std::string text;
	size_t pageCount = document->pages();
	for(size_t i = 0; i < pageCount; ++i)
	{
		poppler::page* page = document->create_page(i);
		poppler::rectf rect = page->page_rect();
		rect.set_top(rect.height()*0.02);
		rect.set_bottom(rect.height()*0.96);
		text.append(page->text(rect, poppler::page::non_raw_non_physical_layout).to_latin1());
		text.append("\n");
		delete page;
	}
	return text;
}

static bool isInvalid(const char in)
{
	if((in < 32 || in > 126) && in != '\n' && in != '\r')
		return true;
	else
		return false;
}

static bool isNewline(const char in)
{
	return in == '\n';
}

static bool isNotWitespace(const char in)
{
	return !(in == ' ' || in == '\t');
}

void removeDuplicate(std::string& in, char dupCh)
{
	for(size_t i = 0; i < in.size()-1; ++i)
	{
		if(in[i] == dupCh && in[i+1] == dupCh)
		{
			in.erase(in.begin()+i);
			--i;
		}
	}
}

std::string processDocumentText(const std::string& in)
{
	std::string out;
	std::vector<std::string> sentances = tokenize(in, ".?!:", '\0', '\0', true);
	for(std::string& sentance : sentances)
	{
		std::remove_if(sentance.begin(), sentance.end(), &isInvalid);
		if(sentance.size() < 4)
			continue;
		if(sentance[0] == '\n')
			out.push_back('\n');
		if(sentance[1] == '\n')
			out.push_back('\n');
		std::replace(sentance.begin(), sentance.end(), '\n', ' ');
		sentance.erase(sentance.begin(), std::find_if(sentance.begin(), sentance.end(), &isNotWitespace));
		if(out.back() != '\n' && out.back() != ' ')
			out.push_back(' ');
		out.append(sentance);
	}
	out.push_back('\n');
	removeDuplicate(out, '.');
	return out;
}

int main(int argc, char** argv)
{
	Log::level = Log::INFO;
	poppler::set_debug_error_function(dropMessage, nullptr);
	std::string outputFile = "-";

	if(argc < 2)
	{
		printUsage(argc, argv);
		return 0;
	}
	else if(argc > 2)
	{
		outputFile = argv[2];
	}

	if(argc > 3)
	{
		Log(Log::WARN)<<"This application will process only one directory or file at a time. Will process: "<<argv[1];
	}

	std::ostream* outputStream;
	std::fstream file;
	if(outputFile != "-")
	{
		file.open(outputFile, std::ios_base::out);
		if(!file.is_open())
		{
			Log(Log::ERROR)<<"could not open "<<outputFile<<" for writeing";
			return 2;
		}
		outputStream = &file;
	}
	else
	{
		outputStream = &std::cout;
	}

	std::filesystem::path dirPath(argv[1]);
	if(std::filesystem::is_regular_file(dirPath))
	{
		Log(Log::INFO)<<"Processing "<<dirPath;
		poppler::document* document = poppler::document::load_from_file(dirPath);
		if(!document)
		{
			Log(Log::WARN)<<"Could not load "<<dirPath;
			return 1;
		}

		std::string text = getText(document);
		text = processDocumentText(text);
		*outputStream<<text;

		delete document;
	}
	else if(std::filesystem::is_directory(dirPath))
	{
		for(const std::filesystem::directory_entry& entry : std::filesystem::directory_iterator(dirPath))
		{
			if((entry.is_regular_file() || entry.is_symlink()) && entry.path().extension() == std::string(".pdf"))
			{
				Log(Log::INFO)<<"Processing "<<entry.path();
				poppler::document* document = poppler::document::load_from_file(entry.path());
				if(!document)
				{
					Log(Log::WARN)<<"Could not load "<<entry.path();
					continue;
				}

				std::string text = getText(document);
				text = processDocumentText(text);

				*outputStream<<text;
				*outputStream<<"\n=====\n";

				delete document;
			}
		}
	}
	else
	{
		Log(Log::ERROR)<<dirPath<<" must be a valid directory";
		return 1;
	}

	if(file.is_open())
		file.close();

	return 0;
}
